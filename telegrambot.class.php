<?php
require_once __DIR__ . '/utils.php';
require_once __DIR__ . '/db.class.php';
require_once __DIR__ . '/user.class.php';
require_once __DIR__ . '/gravityforms.class.php';
require_once __DIR__ . '/farazsms.class.php';
require_once __DIR__ . "/vendor/autoload.php";

class TelegramBot {
  private $bot_token;
  private $user;
  private $chat_id;
  private $first_name;
  private $last_name;
  private $text;
  private $message_id;
  private $callback_id;
  private $bot_handler;
  private $page_id;
  private $keyboard_map;
  private $db;
  private $gf;
  private $sms;
  public function __construct($bot_token){
    if(empty($bot_token)){
      logger("bot token not set!");
      die();
    }
    $this->bot_token = $bot_token;
    $this->bot_handler = new TuriBot\Client($this->bot_token, false);
    $this->db = new dbconnect();
    $this->keyboard_map = null;
    $this->chat_id = "";
    $this->first_name = "";
    $this->last_name = "";
    $this->text = "";
    $this->message_id = "";
    $this->callback_id = "";
    $this->page_id = 0;
    $this->user = null;
    $this->gf = null;
    $this->sms = null;
  }
  public function get_update(){
    logger("func call: TelegramBot get_update");
    $update = $this->bot_handler->getUpdate();
    // handle message
    if (isset($update->message) or isset($update->edited_message)) {
        $this->message_id = $this->bot_handler->easy->message_id;
        $this->text = arabic_persian_numbers_to_english(
          $this->bot_handler->easy->text);
        $this->chat_id = $this->bot_handler->easy->chat_id;
        $this->first_name = $update->message->chat->first_name;
        $this->last_name = $update->message->chat->last_name;
    }
    /*else{

      // handle callback query
      if (isset($update->callback_query)) {
        $callback_id = $update->callback_query->id;
        $this->chat_id = $update->callback_query->message->chat->id;
        $this->message_id = $update->callback_query->message->message_id;
        $this->text = $update->callback_query->data;
        logger('var callback_query data: '.$this->text);
        // fetch user data from database and authenticate check
        if($this->user_authenticate()){
          // continue here
          if ($this->text === 'sms_add_entry') {
            $this->fetch_page_id('sms');
            $message = 'لطفا شماره موبایل را وارد کنید';
            $this->send_message($message);
          } else {
              logger('error on get update message');
              die();
          }
        } else {
        }
      }
    } */
    // is user exist in db
    $this->handle_request();
  }
  private function handle_request(){
    logger("func call: TelegramBot handle_request");
    $this->user = $this->get_user();
    // parse text
    if ($this->text == '/start') {
      $this->fetch_page_id('start');
      $message = 'به ربات دستیار مهرآفرین خوش آمدید'.PHP_EOL.
      'این ربات قراره یکسری کارها رو برای شما آسون تر کنه'.PHP_EOL.
      'یکی از بخش ها رو انتخاب کنید';
      if(!$this->user_authenticate(1)){
        $message .= PHP_EOL.PHP_EOL.'👈 توجه: دسترسی شما محدود شده است'.PHP_EOL;
        $message .= 'درصورت نیاز شناسه زیر را برای مدیر ربات ارسال کنید تا دسترسی باز شود';
        $message .= PHP_EOL.'شناسه شما: '.$this->chat_id;
      }
      $this->send_message($message);
    } else if($this->text == 'بازگشت'){
      $this->return_to_parent_page();
    }else {
      // fetch user data from database and authenticate check
      if($this->user_authenticate(1)){
        $user_page_id = $this->user->get_page_id();
        $keyboard_map_slug = $this->get_keyboard_map('slug');
        if($user_page_id == 0){
          switch($this->text){
            case 'فروشگاه':
              $this->page_id = $keyboard_map_slug['woocommerce']['id'];
              $this->send_message('بخش فروشگاه درحال ساخت می باشد');
              break;
            case 'فرم ها':
              $this->page_id = $keyboard_map_slug['gravity_forms']['id'];
              $message = 'یکی از گزینه های زیر را انتخاب کنید';
              $this->send_message($message);
              break;
            case 'پنل پیامک':
              $this->page_id = $keyboard_map_slug['sms']['id'];
              $message = 'یکی از گزینه های زیر را انتخاب کنید';
              $this->send_message($message);
              break;
            case 'مدیریت' and $this->user_authenticate(2):
              $this->page_id = $keyboard_map_slug['manage']['id'];
              $this->send_message('یکی از گزینه های زیر را انتخاب کنید');
              break;
            default:
              $this->page_id = 0;
              $this->text = 'start';
              $message = 'عمل مورد نظر یافت نشد'.PHP_EOL.'مجددا تلاش کنید';
              $this->send_message($message);
              break;
          }
        } else if($user_page_id == $keyboard_map_slug['sms']['id']){
          $this->page_id = $keyboard_map_slug['sms']['id'];
           switch($this->text){
             case 'افزودن شماره':
               $message = 'شماره را وارد کنید';
               $message .= PHP_EOL.'👈 توجه: شماره در دفترچه تلفن وبینار اعتماد به نفس ذاتی ذخیره خواهد شد';
               $this->send_message($message);
               break;
             case 'لیست شماره ها':
               $this->sms = new faraz_sms();
               $entries = explode(',',
                preg_replace('/"|\]|\[|\s/', '', $this->sms->get_entries(300300)));
               $message = '';
               for($index = 0; $index < count($entries); $index++){
                 $message .= ($index + 1).' => '.
                  str_replace('+98', '0', $entries[$index]).PHP_EOL.PHP_EOL;
               }
               $this->send_message($message);
               $this->send_message('👆 اینا شماره های دفترچه وبینار اعتماد به نفس ذاتی است');
               break;
             case (preg_match('/09[0-9]{9}/', $this->text) ? true : false) &&
              $this->user->get_text() == 'افزودن شماره':
               $this->fetch_page_id('sms');
               $this->sms = new faraz_sms();
               $response = $this->sms->add_entry(300300, $this->text);
               if($response['status']['code'] == 101){
                 $message = 'این شماره قبلا در دفترچه تلفن ذخیره شده است';
                 $this->send_message($message);
               } else if($response['status']['code'] == 0){
                 $message = 'شماره با موفقیت در دفترچه تلفن ذخیره شد';
                 $this->send_message($message);
               } else {
                 $message = 'خطایی رخ داده است، با مدیر ربات در میان بگذارید';
                 $this->send_message($message);
               }
               break;
             default:
               $this->text = 'sms';
               $message = 'عمل مورد نظر یافت نشد'.PHP_EOL.'مجددا تلاش کنید';
               $this->send_message($message);
               break;
           }
         } else if($user_page_id == $keyboard_map_slug['gravity_forms']['id']){
           $this->page_id = $keyboard_map_slug['gravity_forms']['id'];
           switch($this->text){
             case 'افزودن مشتری':
               $message = 'اطلاعات مشتری را به شکل زیر وارد کنید'.PHP_EOL.
               PHP_EOL.'شماره موبایل+نام+نام خانوادگی'.PHP_EOL.PHP_EOL.
               'مثال:'.PHP_EOL.PHP_EOL.'09123456789+علی+احمدی'.PHP_EOL.
               PHP_EOL.'👈نکته: اولین فیلد شماره موبایل است';
               $message .= PHP_EOL.PHP_EOL.'👈 توجه: مشخصات در فرم ثبت نام وبینار اعتماد به نفس ذاتی ذخیره خواهد شد';
               $this->send_message($message);
               break;
             case 'ورودی ها':
               $this->send_message('به زودی...');
               break;
             case 'پرداخت های موفق':
               $this->gf = new gravity_forms();
               $response = $this->gf->get_paid_entries(12, '3,4,8', 1000);
               $message = "";
               $index = 1;
               foreach ($response['entries'] as $entry) {
                 $message .= $index.PHP_EOL.$entry['_labels']['3'].' '.
                  $entry['3'].PHP_EOL.$entry['_labels']['4'].' '.$entry['4'].
                  PHP_EOL.$entry['_labels']['8'].' '.$entry['8'].PHP_EOL.
                  'تاریخ پرداخت: '.$entry['date_created'].PHP_EOL.
                  '------------------------'.PHP_EOL;
                 if($index % 20 == 0){
                   $this->send_message($message);
                   $message = "";
                 }
                 $index = $index + 1;
               }
               if(!empty($message)){
                 $this->send_message($message);
               }
               $this->send_message('👆 اینا لیست افراد ثبت نامی وبینار اعتماد به نفس ذاتی است');
               break;
             case (preg_match('/^09[0-9]{9}/', $this->text) ? true : false) &&
              $this->user->get_text() == 'افزودن مشتری':
               $this->gf = new gravity_forms();
               $fields = preg_split('/\+/', preg_replace('/(\s+)\+(\s+)/', '+', $this->text));
               if($fields == false || count($fields) != 3){
                 $message = 'خطا در دریافت مشخصات مشتری'.PHP_EOL.
                  'لطفا طبق الگو ارسال کنید ';
                 $this->send_message($message);
               } else{
                 $customer_phone = $fields[0];
                 $customer_first_name = $fields[1];
                 $customer_last_name = $fields[2];
                 $data = array(
                   '8' => $customer_phone,
                   '3' => $customer_first_name,
                   '4' => $customer_last_name
                   );
                 $response = $this->gf->add_entry(12, $data);
                 $message = isset($response['data']['status']) ?
                  'ثبت مشتری با خطا مواجه شد' : 'ثبت مشتری با موفقیت انجام شد';
                 $this->send_message($message);
               }
               $this->text = 'افزودن مشتری';
               break;
             default:
               $this->text = 'gravity_forms';
               $message = 'عمل مورد نظر یافت نشد'.PHP_EOL.'مجددا تلاش کنید';
               $this->send_message($message);
               break;
           }
         } else if($user_page_id == $keyboard_map_slug['woocommerce']['id']){
             $this->page_id = $keyboard_map_slug['woocommerce']['id'];
             switch($this->text){
               case 'سفارشات':
                 $this->send_message('به زودی...');
                 break;
               default:
                 $this->text = 'woocommerce';
                 $message = 'عمل مورد نظر یافت نشد'.PHP_EOL.'مجددا تلاش کنید';
                 $this->send_message($message);
                 break;
             }
         } else if($user_page_id == $keyboard_map_slug['manage']['id']
            && $this->user_authenticate(2)){
              $this->page_id = $keyboard_map_slug['manage']['id'];
              switch($this->text){
                case 'افزودن دسترسی':
                  $message = 'برای این کار طبق فرمت زیر عمل کنید'.PHP_EOL.
                  PHP_EOL.'شماره شناسه-نام-نام خانوادگی'.PHP_EOL.PHP_EOL.
                  'مثال:'.PHP_EOL.PHP_EOL.'123456789-علی-احمدی'.PHP_EOL.
                  PHP_EOL.'👈نکته: اولین فیلد شماره شناسه است';
                  $this->send_message($message);
                  break;
                case (preg_match('/^([0-9]+)-/', $this->text) ? true : false)
                 && $this->user->get_text() == 'افزودن دسترسی':
                  $fields = preg_split('/-/', preg_replace('/(\s+)-(\s+)/', '-',
                   $this->text));
                  $new_user = $this->get_user($fields[0]);
                  $new_user_first_name = $new_user->get_first_name();
                  $new_user_last_name = $new_user->get_last_name();
                  if($new_user){
                    $keys = array('first_name', 'last_name', 'role_id', 'page_id');
                    $values = array($fields[1], $fields[2], 1, 0);
                    $new_user->update($keys, $values);
                  } else {
                    $new_user = new user($fields[0], $fields[1], $fields[2],
                     1, 0);
                    $new_user->insert();
                  }
                  $message = 'دسترسی کاربر فعال شد'.PHP_EOL.
                   'مشخصات کاربر قبل از بروزرسانی'.PHP_EOL.
                   'نام: '.$new_user_first_name.PHP_EOL.
                   'نام خانوادگی: '.$new_user_last_name.PHP_EOL.'.';
                  $this->send_message($message);
                  break;
                default:
                  $this->text = 'manage';
                  $message = 'عمل مورد نظر یافت نشد'.PHP_EOL.'مجددا تلاش کنید';
                  $this->send_message($message);
                  break;
              }
         }
      } else{
        $message = 'شما مجوز استفاده از این بخش را ندارید.'.PHP_EOL;
        $message .= 'درصورت نیاز شناسه زیر را برای مدیر ربات ارسال کنید تا دسترسی باز شود';
        $message .= PHP_EOL.'شناسه شما: '.$this->chat_id;
        $this->send_message($message);
      }
    }
    // user update
    if(!$this->user){
      $this->user = new user($this->chat_id, $this->first_name, $this->last_name, 0,
        $this->page_id, $this->text);
      $this->user->insert();
    } else{
        $keys = array('page_id', 'text');
        $values = array($this->page_id, $this->text);
        $this->user->update($keys, $values);
      }
  }
  private function return_to_parent_page(){
    logger('func call: TelegramBot return_to_parent_page');
    $this->user = $this->get_user();
    $this->keyboard_map = $this->get_keyboard_map();
    if($this->user->get_page_id() != 0){
        $this->page_id = $this->keyboard_map[$this->user->get_page_id()]['parent_id'];
    } else {
      $this->page_id = 0;
    }
    if($this->page_id == 0){
      $this->text = '/start';
    } else {
        $this->text = $this->keyboard_map[$this->page_id]['name'];
    }
    $this->handle_request();
  }
  private function fetch_page_id($page_slug){
    logger('func call: TelegramBot fetch_page_id');
    if($page_slug == 'start'){
      $this->page_id = 0;
    } else {
      $db_data = $this->db->select('keyboard_map', array('id'),
       array('slug' => $page_slug), true);
      if(!empty($db_data)){
        $this->page_id = $db_data['id'];
      } else{
        $this->page_id = '';
      }
    }
  }
  private function user_authenticate($role_id = 1){
    logger('func call TelegramBot user_authenticate');
    $this->user = $this->get_user();
    return $this->user && $this->user->get_role_id() >= $role_id;
  }
  private function get_user($chat_id = ""){
    logger('func call TelegramBot get_user');
    if(empty($chat_id)){
      $chat_id = $this->chat_id;
    }
    if($chat_id == $this->chat_id && $this->user){
      return $this->user;
    } else{
        $user_data = $this->db->select('users', '*',
        array('chat_id' => $chat_id), true);
      if(!empty($user_data)){
        $user = new user($user_data['chat_id'], $user_data['first_name'],
        $user_data['last_name'], $user_data['role_id'], $user_data['page_id'],
        $user_data['text']);
        return $user;
      } else {
         logger('user not exist in database!');
         return null;
      }
    }
  }
  private function get_keyboard_map($field = 'id'){
    logger('func call: TelegramBot get_keyboard_map');
    $keyboard = $this->db->select('keyboard_map');
    if(!$keyboard){
      logger('failed to fetch keyboard map');
      die();
    }
    $keyboard_map = array();
    $this->user = $this->get_user();
    $user_role_id = $this->user ? $this->user->get_role_id() : 0;
    foreach ($keyboard as $key) {
      if($key['min_role_id'] <= $user_role_id){
        $index = $key[$field];
        unset($key[$field]);
        $keyboard_map[$index] = $key;
      }
    }
    return $keyboard_map;
  }
  private function get_keyboard($inline_mode = false, $one_time_keyboard = false){
    logger('func call: TelegramBot get_keyboard');
    $this->keyboard_map = $this->get_keyboard_map();
    if($this->page_id == 0){
      $parent_id = 0;
      $inline_mode = false;
    } else{
      if(!empty($this->keyboard_map[$this->page_id])){
          $parent_id = $this->page_id;
      } else {
        logger('keyboard menu not exist!');
        return null;
      }
    }
    $keyboard = array();
    $keyboard_row = array();
    // inline keyboard
    if($inline_mode){
      foreach($this->keyboard_map as $id => $keymap){
        if($keymap['parent_id'] == $parent_id){
          array_push($keyboard_row, array(
            'text' => $keymap['name'],
            'callback_data' => $keymap['slug']));
        }
      }
      $keyboard["inline_keyboard"] = [array_reverse($keyboard_row)];
    } else{
      foreach($this->keyboard_map as $id => $keymap){
        if($keymap['parent_id'] == $parent_id){
          array_push($keyboard_row, $keymap['name']);
        }
      }
      if($parent_id != 0){
        array_push($keyboard_row, 'بازگشت');
      }
      $keyboard = array(
        "keyboard" => [array_reverse($keyboard_row)],
        "resize_keyboard" => true,
        "one_time_keyboard" => $one_time_keyboard
      );
    }
    return $keyboard;
  }
  private function send_message($message, $keyboard_inline_mode = false,
   $one_time_keyboard = false){
    logger('func call: TelegramBot send_message');
    $keyboard = $this->get_keyboard($keyboard_inline_mode, $one_time_keyboard);
    logger('keyboard'. json_encode($keyboard));
    $this->bot_handler->sendMessage($this->chat_id, $message, null, null, null, null,
     null, null, $keyboard);
  }
}

?>
