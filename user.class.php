<?php
require_once __DIR__ . '/utils.php';
require_once __DIR__. '/db.class.php';

class user {
  private $chat_id;
  private $first_name;
  private $last_name;
  private $role_id;
  private $page_id;
  private $text;
  private $db;
  public function __construct($chat_id = "", $first_name = "", $last_name = "",
  $role_id = 0, $page_id = "", $text = ""){
    $this->chat_id = $chat_id;
    $this->first_name = $first_name;
    $this->last_name = $last_name;
    $this->role_id = $role_id;
    $this->page_id = $page_id;
    $this->text = $text;
    $this->db = new dbconnect();
  }
  public function update($keys, $values){
    logger('func call: user update');
    $conditions = array('chat_id' => $this->chat_id);
    $this->db->update('users', $keys, $values, $conditions);
  }
  public function insert(){
    logger('func call: user insert');
    $keys = array('chat_id', 'role_id', 'first_name',
     'last_name', 'page_id', 'text');
     logger('var first_name: '.$this->first_name);
     logger('var last_name: '.$this->last_name);
    $values = array($this->chat_id, $this->role_id, $this->first_name,
     $this->last_name, $this->page_id, $this->text);
    $this->db->insert('users', $keys, $values);
  }
  public function get_role_id(){ return $this->role_id; }
  public function get_text(){ return $this->text; }
  public function get_page_id(){ return $this->page_id; }
  public function get_first_name(){ return $this->first_name; }
  public function get_last_name(){ return $this->last_name; }
}

?>
