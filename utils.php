<?php
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/jdf.php';
function logger($message){
  global $debug_mode;
  if($debug_mode){
    $logger_file_path = __DIR__.'/log.txt';
    $myfile = fopen($logger_file_path, "a") or die("Unable to open file!");
    date_default_timezone_set("Asia/Tehran");
    $now = date("Y-m-d h:i:sa");
    fwrite($myfile, "\n".$now."\t".$message);
    fclose($myfile);
  }
}
function arabic_persian_numbers_to_english($string) {
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];
    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);
    return $englishNumbersOnly;
}
function convert_datetime($datetime, $source_timezone){
  $utc_imezone = new DateTimeZone( 'UTC' );
  $dt = new DateTime($datetime, $utc_imezone);
  $new_timezone = new DateTimeZone('Asia/Tehran');
  $dt->setTimeZone($new_timezone);
  $dt = preg_split("/[-\s:]/", $dt->format('Y-m-d H:i:s'));
  logger('var dt => '.json_encode( $dt));
  $date = gregorian_to_jalali(intval(dt[0]),intval(dt[1]),intval(dt[2]),'/');
  return $date.' '.$dt[3].':'.$dt[4].':'.$dt[5];
}

?>
