# Web assistance Telegram Bot

Working with Gravity Forms, Woocommerce, ... in Telegram Bot!

## Requirements

Use the package manager [composer](https://getcomposer.org/) to install Turi Bot.

```bash
composer require davtur19/turibot
```

Use the package manager [composer](https://getcomposer.org/) to install php curl class.

```bash
composer require php-curl-class/php-curl-class
```

## Setup

copy or rename config.sample.php to config.php and fill config variables
