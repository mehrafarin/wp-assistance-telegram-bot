<?php
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/utils.php';
class dbconnect {
  private $db_handler;
  public function __construct(){
    global $db_hostname, $db_user, $db_pass, $db_name;
    $dbconn = new mysqli($db_hostname, $db_user, $db_pass, $db_name);
    if($dbconn->connect_error){
      logger(mysqli_error('can not established connect to database!'));
    } else {
      mysqli_set_charset($dbconn, 'utf8');
      $this->db_handler = $dbconn;
    }
  }
  public function __destruct(){
    $this->db_handler->close();
  }
  public function insert($table, $keys, $values, $noerror = false){
    $sql = 'INSERT INTO '.$table." (`".implode("`,`", $keys)."`) VALUES ('".
    implode("','", $values)."')";
    logger("var sql: $sql");
    if ($this->db_handler->query($sql) === true) {
      logger("new record inserted in database successfully");
      return true;
    } else {
      logger("Error: " . $sql . "<br>" . $this->db_handler->error);
      return false;
    }
  }
  public function select($table, $fields = '*', $conditions = null,
   $single_mode = false){
    if($fields != '*'){ $fields = implode('`,`', $fields); }
    $sql = "SELECT $fields FROM $table";
    if(isset($conditions)){
      array_walk($conditions, create_function('&$i,$k','$i="`$k`=\"$i\"";'));
      $sql.= ' WHERE '. implode(' and ', $conditions);
    }
    if($single_mode){
      $sql.= ' LIMIT 1';
    }
    logger('var sql: '.$sql);
    $data = array();
    $result = $this->db_handler->query($sql);
    if ($result->num_rows > 0) {
      if($single_mode){
        $data = $result->fetch_assoc();
      } else {
          while($row = $result->fetch_assoc()) {
            array_push($data, $row);
          }
      }
    }
    return $data;
  }
  public function update($table, $keys, $values, $conditions = null){
    $dataset = array_combine($keys, $values);
    array_walk($dataset, create_function('&$i,$k','$i="`$k`=\"$i\"";'));
    $sql = 'UPDATE '.$table.' SET '.implode(',', $dataset);
    if(isset($conditions)){
      array_walk($conditions, create_function('&$i,$k','$i="`$k`=\"$i\"";'));
      $sql.= ' WHERE '. implode(' and ', $conditions);
    }
    logger('var sql: '.$sql);
    if ($this->db_handler->query($sql) === true) {
      logger("record updated in database successfully");
    } else {
      logger("Error: " . $sql . "<br>" . $this->db_handler->error);
    }
  }
  public function delete($table, $conditions){
    $sql = 'DELETE FROM $table WHERE '.http_build_query($conditions);
    logger('var sql: '.$sql);
    if ($this->db_handler->query($sql) === true) {
      logger("record deleted from database successfully");
    } else {
      logger("Error: " . $sql . "<br>" . $this->db_handler->error);
    }
  }
}
?>
