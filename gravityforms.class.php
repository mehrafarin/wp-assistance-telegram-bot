<?php
require_once __DIR__ . '/utils.php';
require_once __DIR__ . '/db.class.php';
require_once __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';
use Curl\Curl;

class gravity_forms {
  private $api_key;
  private $api_secret;
  private $api_url;
  private $source_url;
  public function __construct($api_key = "", $api_secret = "", $api_url = ""){
    logger('func call: gravity_forms construct');
    global $consumer_key, $consumer_secret, $consumer_url;
    if(empty($api_key) || empty($api_secret || empty($api_url))){
      logger("api key or api secret not set, loaded from config!");
      if(empty($consumer_key) || empty($consumer_secret) || empty($consumer_url)){
        logger("consumer key/consumer secret/consumer url not exist in config");
        die();
      }
      $api_key = $consumer_key;
      $api_secret = $consumer_secret;
      $api_url = $consumer_url;
    }
    $this->api_key = $api_key;
    $this->api_secret = $api_secret;
    $this->api_url = $api_url;
    $this->source_url = $source_url;
  }
  private function request($path, $method = "get", $data = ""){
    logger('func call: gravity_forms request');
    $curl = new Curl();
    $curl->setDefaultJsonDecoder($assoc = true);
    $curl->setHeader('Content-Type', 'application/json');
    $curl->setBasicAuthentication($this->api_key, $this->api_secret);
    if(preg_match("/get/i", $method)){
      $curl->get($this->api_url.$path, $data);
    } else if(preg_match("/post/i", $method)){
      $curl->post($this->api_url.$path, $data);
    }
    if ($curl->error) {
        logger('Error: ' . $curl->errorCode . ': ' . $curl->errorMessage);
        return null;
    } else {
        return $curl->response;
    }
  }
  public function get_forms(){
    logger('func call: gravity_forms get_forms');
  }
  public function get_entries(){
    logger('func call: gravity_forms get_entries');
    return $this->request('/v2/entries', 'get', [
      'form_ids[0]'=> $form_id
    ]);
  }
  public function get_paid_entries($form_id, $_field_ids = "", $page_size = 20,
    $paging_offset = 0){
    logger('func call: gravity_forms get_paid_entries');
    $_field_ids.= ',date_created';
    return $this->request('/v2/entries', 'get', [
      'form_ids[0]'=> $form_id,
      '_labels' => 1,
      'search' => '{"field_filters": [{"key":"payment_status","value":"Paid","operator":"is"}]}',
      '_field_ids' => $_field_ids,
      'paging[page_size]' => $page_size,
      'paging[offset]' => $paging_offset,
      'sorting[key]' => 'date_created',
      'sorting[direction]'=> 'ASC',
      'sorting[is_numeric]]' => 'true'
    ]);
  }
  public function add_entry($form_id, $append_data){
    logger('func call: gravity_forms add_entry');
    $data = array(
      "form_id" => $form_id,
      "date_created" => date("Y/m/d H:i:s"),
      "is_starred" => 1,
      "is_read" => 1,
      "ip" => "::1",
      "source_url" => "https://t.me",
      "currency" => "IRT",
      "created_by" => 1,
      "user_agent" => "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0",
      "status"  => "active",
      "payment_method" => "cart-to-cart",
      "payment_status" => "Paid");
    $data = $data + $append_data;
    return $this->request('/v2/entries', 'post', $data);
  }
}

?>
