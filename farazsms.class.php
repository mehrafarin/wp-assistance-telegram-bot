<?php
require_once __DIR__ . '/utils.php';
require_once __DIR__ . '/db.class.php';
require_once __DIR__ . '/config.php';
require __DIR__ . '/vendor/autoload.php';
use Curl\Curl;

class faraz_sms {
  private $username;
  private $password;
  public function __construct($username = "", $password = ""){
    global $consumer_username, $consumer_password, $consumer_url;
    if(empty($username) || empty($password)){
      if(empty($consumer_username) || empty($consumer_password) || empty($consumer_url)){
        logger("can't get sms username/password/url");
        die();
      }
      $username = $consumer_username;
      $password = $consumer_password;
    }
    $this->username = $username;
    $this->password = $password;
  }
  private function request($api_url, $method = "get", $data = ""){
    logger('func call: faraz_sms request');
    $curl = new Curl();
    $curl->setDefaultJsonDecoder($assoc = true);
    $curl->setHeader('Content-Type', 'application/json');
    $curl->setBasicAuthentication($this->$username, $this->$password);
    if(preg_match("/get/i", $method)){
      $curl->get($api_url, $data);
    } else if(preg_match("/post/i", $method)){
      $curl->post($api_url, $data);
    }
    if ($curl->error) {
        logger('Error: ' . $curl->errorCode . ': ' . $curl->errorMessage);
        return null;
    } else {
        logger('response:'.json_encode($curl->response));
        return $curl->response;
    }
  }
  public function get_entries($phonebook_id){
    logger('func call: faraz_sms get_entries');
    $data = array(
      "op" => "booknumberlist",
    	"uname" => $this->username,
    	"pass" =>  $this->password,
    	"phonebook_id" => $phonebook_id
    );
    return $this->request('https://ippanel.com/api/select', 'post', json_encode($data));
  }
  public function add_entry($phonebook_id, $phone_number){
    logger('func call: faraz_sms add_entry');
    $data = array(
      "op" => "phoneBookAdd",
    	"uname" => $this->username,
    	"pass" =>  $this->password,
      "number" => $phone_number,
    	"phoneBookId" => $phonebook_id
    );
    return $this->request('https://ippanel.com/api/select', 'post', json_encode($data));
  }
}

?>
